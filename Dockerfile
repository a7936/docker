FROM debian

WORKDIR /app

RUN apt-get update --yes
RUN apt install libaa-bin --yes

COPY ./build.sh ./build.sh

ENTRYPOINT bash ./build.sh