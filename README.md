![cowsay](./cowsay.PNG)


## Table of Contents

- [Sections](#sections)
  - [Title](#title)
  - [Short Description](#short-description)
  - [Known errors](#erros)
  - [Install](#install)
  - [Usage](#usage)
  - [Maintainers](#maintainers)
  - [Contributing](#contributing)

## Sections

### Title
Lesson Task: 2.2 Docker

### Short Description
Docker installation with debian image. 

### Install

Make sure you have docker installed.  

 Plugins:  
  app: Docker App (Docker Inc., v0.9.1-beta3)  
  buildx: Docker Buildx (Docker Inc., v0.7.1)  
  compose: Docker Compose (Docker Inc., v2.2.3)  
  scan: Docker Scan (Docker Inc., v0.16.0)  

Server:  
 Server Version: 20.10.12  

### Usage

docker build . -t debian/aafire  
docker run -it debian/aafire

### Docker usage 
Grafana  
https://hub.docker.com/r/grafana/grafana  
Grafana is visualization dashboard for embedded systems.  
One of the best tools when building and using iot systems with different IO data coming throught.  

You can also put a docker inside a docker.  

### Maintainer(s)

Antti Lehtosalo @mediator246 

### Contributing

Antti Lehtosalo @mediator246  